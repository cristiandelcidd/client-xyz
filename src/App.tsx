import { FC } from "react";
import { RouterProvider } from "react-router-dom";
import { Navbar } from "./components";

import routes from "./router";

const App: FC = () => {
  return <RouterProvider router={routes} />;
};

export default App;
