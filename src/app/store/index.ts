import { configureStore } from "@reduxjs/toolkit";

import { branchOfficeReducer } from "../reducers";

const store = configureStore({
  reducer: {
    branchOffice: branchOfficeReducer,
  },
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
