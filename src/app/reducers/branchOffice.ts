import { createSlice } from "@reduxjs/toolkit";

import { BranchOffice } from "../../@types/branch_office";

const initialState: BranchOffice[] =
  JSON.parse(localStorage.getItem("list")!) || [];

export const branchOfficeSlice = createSlice({
  name: "branchOffice",
  initialState,
  reducers: {
    createBranchOffice: (state, action) => {
      state.push(action.payload);

      localStorage.setItem("list", JSON.stringify(state));
    },
    updateBranchOffice: (state, action) => {
      console.log(state);
    },
    deleteBranchOffice: (state, action) => {
      const branchOfficeFound = state.find(
        (branchOffice) => branchOffice.id === action.payload
      );

      if (branchOfficeFound) {
        branchOfficeFound.isActive = false;
        branchOfficeFound.deleteAt = new Date().toLocaleString();
      }
    },
  },
});

export const { createBranchOffice, updateBranchOffice, deleteBranchOffice } =
  branchOfficeSlice.actions;

export default branchOfficeSlice.reducer;
