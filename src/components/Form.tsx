import { FC } from "react";
import { useLocation } from "react-router-dom";

import Button from "./Button";
import FormGroup from "./FormGroup";

import { useForm } from "../hooks";

const Form: FC = () => {
  const location = useLocation();

  const {
    branchOfficeName,
    adminName,
    phoneNumber,
    address,
    fax,
    orderPerMonth,
    pathnameLocationVerification,
    handleSubmit,
    handleInputChange,
  } = useForm(location);

  return (
    <>
      <h3 className="mt-3">
        {pathnameLocationVerification() ? "Crear" : "Actualizar"} Sucursal
      </h3>
      <form onSubmit={handleSubmit}>
        <FormGroup
          type={"text"}
          label="Nombre de la sucursal"
          name="branchOfficeName"
          value={branchOfficeName}
          handleChange={handleInputChange}
          required
        />
        <FormGroup
          type={"text"}
          label="Nombre del administrador"
          name="adminName"
          value={adminName}
          handleChange={handleInputChange}
          required
        />
        <FormGroup
          type={"tel"}
          label="Teléfono"
          name="phoneNumber"
          value={phoneNumber}
          handleChange={handleInputChange}
          required
        />
        <FormGroup
          type={"text"}
          label="Dirección"
          name="address"
          value={address}
          handleChange={handleInputChange}
          required
        />
        <FormGroup
          type={"text"}
          label="Fax"
          name="fax"
          value={fax}
          handleChange={handleInputChange}
        />
        <FormGroup
          type={"number"}
          label="Número de pedidos al mes"
          name="orderPerMonth"
          value={orderPerMonth}
          handleChange={handleInputChange}
        />

        <Button
          text={pathnameLocationVerification() ? "Crear" : "Actualizar"}
          className={
            pathnameLocationVerification() ? "btn-primary" : "btn-success"
          }
        />
      </form>
    </>
  );
};

export default Form;
