import { FC } from "react";

import { BranchOffice } from "../@types/branch_office";
import BranchOfficeItem from "./BranchOfficeItem";

interface Props {
  list: BranchOffice[];
}

const BranchOfficeList: FC<Props> = ({ list }) => {
  console.log(list);
  return (
    <div
      className="mt-3 d-grid gap-1"
      style={{ gridTemplateColumns: "repeat(3, 33%)" }}
    >
      {list.map((branchOffice) => (
        <BranchOfficeItem key={branchOffice.id} branchOffice={branchOffice} />
      ))}
    </div>
  );
};

export default BranchOfficeList;
