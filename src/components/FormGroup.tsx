import { ChangeEvent, FC, HTMLInputTypeAttribute } from "react";

interface Props {
  label: string;
  type: HTMLInputTypeAttribute;
  name: string;
  value: any;
  required?: boolean;
  handleChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

const FormGroup: FC<Props> = ({
  label,
  type,
  name,
  value,
  handleChange,
  required = false,
}) => {
  return (
    <div className="mb-3">
      <label htmlFor={name} className="form-label">
        {label} <small>{required && "(Obligatorio)"}</small>
      </label>
      <input
        onChange={handleChange}
        type={type}
        autoComplete={"off"}
        name={name}
        value={value}
        className="form-control"
        id={name}
        required={required}
        min={0}
      />
    </div>
  );
};

export default FormGroup;
