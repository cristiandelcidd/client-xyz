import { FC } from "react";
import { useDispatch } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import Swal from "sweetalert2";

import { BranchOffice } from "../@types/branch_office";
import { deleteBranchOffice } from "../app/reducers/branchOffice";

interface Props {
  branchOffice: BranchOffice;
}

const BranchOfficeItem: FC<Props> = ({ branchOffice }) => {
  const dispatch = useDispatch();
  const location = useLocation();

  console.log(location.pathname.match(/sucursales-eliminadas/i));

  const {
    address,
    adminName,
    branchOfficeName,
    orderPerMonth,
    phoneNumber,
    fax,
    id,
    isActive,
  } = branchOffice;

  const handleDeleteBranchOffice = () => {
    Swal.fire({
      title: "Estás seguro?",
      icon: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, eliminarlo!",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteBranchOffice(id));
        Swal.fire("Eliminado!", "", "success");
      }
    });
  };

  return (
    <div className="card">
      <div className="card-body d-grid" style={{ alignItems: "center" }}>
        <div className="d-grid">
          <h5 className="card-title">
            Nombre de la sucursal: {branchOfficeName}
          </h5>
        </div>
        <ul>
          <li>Dirección: {address}</li>
          <li>Administrador: {adminName}</li>
          <li>Número de teléfono: {phoneNumber}</li>
        </ul>
        {orderPerMonth > 0 && (
          <p className="card-text">Orden por mes: {orderPerMonth}</p>
        )}
        <div
          className="d-grid gap-1"
          style={{
            gridTemplateColumns: "repeat(2, calc(50% - 0.1rem))",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Link to={`/actualizar-sucursal/${id}`} className="btn btn-primary">
            Editar
          </Link>
          {!location.pathname.match(/sucursales-eliminadas/i) ? (
            <button
              className="btn btn-danger ml-3"
              onClick={handleDeleteBranchOffice}
            >
              Eliminar
            </button>
          ) : (
            <small className="m-0 text-center">Eliminado</small>
          )}
        </div>
      </div>
    </div>
  );
};

export default BranchOfficeItem;
