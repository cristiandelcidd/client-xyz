import { FC } from "react";

interface Props {
  text: string;
  className?: string;
}

const Button: FC<Props> = ({ text, className = "" }) => {
  return (
    <button type="submit" className={`btn ${className}`}>
      {text}
    </button>
  );
};

export default Button;
