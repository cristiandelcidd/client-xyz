import page_not_found from "../assets/page_not_found.svg";

import { Layout } from "../components";

const NotFound = () => {
  return (
    <Layout>
      <div className="grid text-center">
        <img src={page_not_found} alt="Page Not Found" />
        <h4>La página no existe o ha dido movida!</h4>
      </div>
    </Layout>
  );
};

export default NotFound;
