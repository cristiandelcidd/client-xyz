import { FC } from "react";

import { Form, Layout } from "../components";

const CreateOrUpdate: FC = () => {
  return (
    <Layout>
      <Form />
    </Layout>
  );
};

export default CreateOrUpdate;
