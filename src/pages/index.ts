import CreateOrUpdate from "./CreateOrUpdate";
import Home from "./Home";
import NotFound from "./NotFound";

export { Home, CreateOrUpdate, NotFound };
