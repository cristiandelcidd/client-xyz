import { FC } from "react";
import { useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";

import { BranchOfficeList, Layout } from "../components";
import { BranchOffice } from "../@types/branch_office";

const Home: FC = () => {
  const branchOfficeState = useSelector<{ branchOffice: BranchOffice }>(
    (state) => state.branchOffice
  ) as BranchOffice[];

  const location = useLocation();

  return (
    <Layout>
      <>
        {location.pathname.match(/sucursales-eliminadas/i)?.length! > 0 &&
        branchOfficeState.filter(
          (branchOffice) => branchOffice.isActive === false
        ).length > 0 ? (
          <div className="mt-4">
            <h3>No hay elementos</h3>
            <Link to={"/crear-sucursal"} className="btn btn-secondary mt-2">
              Crear sucursal
            </Link>
          </div>
        ) : (
          <BranchOfficeList
            list={branchOfficeState.filter(
              (branchOffice) => branchOffice.isActive === false
            )}
          />
        )}

        {location.pathname === "/" &&
        branchOfficeState.filter(
          (branchOffice) => branchOffice.isActive === true
        ).length > 0 ? (
          <div className="mt-4">
            <h3>No hay elementos</h3>
            <Link to={"/crear-sucursal"} className="btn btn-secondary mt-2">
              Crear sucursal
            </Link>
          </div>
        ) : (
          <BranchOfficeList
            list={branchOfficeState.filter(
              (branchOffice) => branchOffice.isActive === true
            )}
          />
        )}
      </>
    </Layout>
  );
};

export default Home;
