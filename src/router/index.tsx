import { createBrowserRouter, Navigate } from "react-router-dom";

import { CreateOrUpdate, Home, NotFound } from "../pages";

const routes = createBrowserRouter([
  { path: "/", element: <Home />, errorElement: <NotFound /> },
  {
    path: "/crear-sucursal",
    element: <CreateOrUpdate />,
    errorElement: <NotFound />,
  },
  {
    path: "/actualizar-sucursal/:id",
    element: <CreateOrUpdate />,
    errorElement: <NotFound />,
  },
  {
    path: "/sucursales-eliminadas",
    element: <Home />,
    errorElement: <NotFound />,
  },
  { path: "*", element: <Navigate to={"/"} replace /> },
]);

export default routes;
