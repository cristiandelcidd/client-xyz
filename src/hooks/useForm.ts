import { FormEvent, useState } from "react";
import { Location } from "react-router-dom";
import { useDispatch } from "react-redux";

import { BranchOffice } from "../@types/branch_office";
import { UseForm } from "../@types/hooks";
import { createBranchOffice } from "../app/reducers/branchOffice";

const useForm = (location: Location): UseForm => {
  const pathnameLocationVerification = (): boolean => {
    return location.pathname.match(/crear-sucursal/i)?.length! > 0;
  };

  const dispatch = useDispatch();

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    dispatch(
      createBranchOffice({
        id: +Math.random().toString().split(".")[1],
        branchOfficeName,
        adminName,
        phoneNumber,
        address,
        fax: fax?.length! > 0 ? fax : null,
        orderPerMonth,
        createAt: new Date().toLocaleString(),
        updatedAt: null,
        deleteAt: null,
        isActive: true,
      })
    );
  };

  const [form, setForm] = useState<BranchOffice>({
    branchOfficeName: "",
    adminName: "",
    phoneNumber: "",
    address: "",
    fax: "",
    orderPerMonth: 0,
  });

  const {
    branchOfficeName,
    adminName,
    phoneNumber,
    address,
    fax,
    orderPerMonth,
  } = form;

  const handleInputChange = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>) => {
    setForm((state) => ({ ...state, [target.name]: target.value }));
  };

  return {
    branchOfficeName,
    adminName,
    phoneNumber,
    address,
    fax,
    orderPerMonth,
    pathnameLocationVerification,
    handleSubmit,
    handleInputChange,
  };
};

export default useForm;
