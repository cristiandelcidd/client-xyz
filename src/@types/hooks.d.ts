export interface UseForm {
  branchOfficeName: string;
  adminName: string;
  phoneNumber: string;
  address: string;
  fax: string | null | undefined;
  orderPerMonth: number;
  pathnameLocationVerification: () => boolean;
  handleSubmit: (event: FormEvent<HTMLFormElement>) => void;
  handleInputChange: ({ target }: React.ChangeEvent<HTMLInputElement>) => void;
}
