// export interface BranchOfficeState {
//   loading: boolean;
//   error: string | null;
//   branchOfficeList: BranchOffice[];
// }

// export interface BranchOfficeSelector {
//   branchOffice: BranchOfficeState;
// }

export interface BranchOffice {
  id?: number;
  branchOfficeName: string;
  adminName: string;
  phoneNumber: string;
  address: string;
  orderPerMonth: number;
  fax?: string | null;
  createAt?: Date;
  updatedAt?: string | null;
  deleteAt?: string | null;
  isActive?: boolean;
}
